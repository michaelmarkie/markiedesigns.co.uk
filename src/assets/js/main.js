(function(){

	var pixel = {
		width: window.innerWidth,
		height: window.innerHeight,
		size: 10,
		x: 0,
		y: 0,
		count: 0,
		count2: 0,
		build: function ( element ) {
			
			this.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
			
			this.svg.setAttribute('viewBox', `0 0 ${this.width} ${this.height}`);
			this.svg.setAttribute('version', '1.1');
			this.svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
			this.svg.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
			this.svg.setAttribute('width', this.width);
			this.svg.setAttribute('height', this.height);
			
			element.appendChild(this.svg);

			this.next();

		},
		next: function () {
			this.count++;
			let rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
			
			rect.setAttribute('width', this.size);
			rect.setAttribute('height', this.size);
			rect.setAttribute('x', this.x);
			rect.setAttribute('y', this.y);
			var max = 80;
			var min = 30;
			rect.setAttribute('fill', 'hsl(127,63%,'+Math.floor(Math.random()*(max-min+1)+min)+'%)');

			this.svg.appendChild(rect);

			if ( this.x >= (this.width - this.size) && this.y <= (this.height - this.size) ) {

				this.y += this.size;
				this.x = 0;
				
				this.next();
				
			} else if ( this.y <= (this.height - this.size) ) {
				
				this.x += this.size;
				
				this.next();
				
			}
		},
		rebuild: function(svg){

			if( this.count2 >= this.count )
				this.count2 = 0;

			this.count2++;

			var 
				max = 80,
				min = 30,
				rand = Math.floor(Math.random()*(max-min+1)+min);
			document
				.querySelector(`svg rect:nth-child(${this.count2})`)
				.setAttribute('fill', `hsl(127,63%,${rand}%)`);

			setTimeout(() => this.rebuild(), 10);
		}
	};

	pixel.build(document.getElementById('svg'));
	pixel.rebuild(document.getElementById('svg'));
})();
